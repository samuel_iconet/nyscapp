
<!doctype html>
<html lang="en">


<!-- Mirrored from colorlib.com/preview/theme/comodo/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 20 May 2019 01:02:43 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="/img/favicon.png" type="image/png">
    <title>Groove </title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/css/bootstrap.css">
    <link rel="stylesheet" href="/vendors/linericon/style.css">
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/vendors/owl-carousel/owl.carousel.min.css">
    <link rel="stylesheet" href="/css/magnific-popup.css">
    <link rel="stylesheet" href="/vendors/nice-select/css/nice-select.css">
    <link rel="stylesheet" href="/vendors/animate-css/animate.css">
    <link rel="stylesheet" href="/vendors/flaticon/flaticon.css">
    <!-- main css -->
    <link rel="stylesheet" href="/css/style.css">
      <style>
         #confirm {
            margin-top: 200px;
            display: none;
            background-color: #F3F5F6;
            color: #000000;
            border: 1px solid #aaa;
            position: fixed;
            width: 300px;
            height: 150px;
            left: 50%;
            margin-left: -100px;
            padding: 10px 20px 10px;
            box-sizing: border-box;
            text-align: center;
         }
         #confirm button {
            background-color: #FFFFFF;
            display: inline-block;
            border-radius: 12px;
            border: 4px solid #aaa;
            padding: 5px;
            text-align: center;
            width: 60px;
            cursor: pointer;
         }
         #confirm .message {
            text-align: left;
         }
      </style>
</head>

<body>
        <!--================Header Menu Area =================-->
    <header class="header_area">
        <div class="main_menu">
            <nav class="navbar navbar-expand-lg navbar-light">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <a class="navbar-brand logo_h" href="/index-2.html"> <img src="img/logo1.png" alt="" style="background: white"> </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                     aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
                        <ul class="nav navbar-nav menu_nav justify-content-center">
                            <li class="nav-item active"><a class="nav-link" href="/index-2.html">Home</a></li>
                            <li class="nav-item"><a class="nav-link" href="/contact.html">Contact</a></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right" style="margin-right: 10px">
                            
                            <li class="nav-item"><a href="/register" class="primary_btn">REGISTER</a></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            
                            <li class="nav-item"><a href="/login" class="primary_btn">LOGIN</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    </header>
    @yield('content')

    <!--================ Start Newsletter Area ================-->
    <section class="newsletter_area">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="newsletter_inner">
                        <h1>Subscribe Our Newsletter</h1>
                        <p>We won’t send any kind of spam</p>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <aside class="newsletter_widget">
                        <div id="mc_embed_signup">
                            <form target="_blank" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01"
                             method="get" class="subscribe_form relative">
                                <div class="input-group d-flex flex-row">
                                    <input name="EMAIL" placeholder="Enter email address" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Your email address'" required="" type="email" data-cf-modified-c62a19f8773acdae2ac8cd25-="">
                                    <button class="btn primary_btn">Subscribe</button>
                                </div>
                            </form>
                        </div>
                    </aside>
                </div>
            </div>
        </div>
    </section>
    <!--================ End Newsletter Area ================-->

    <!--================Footer Area =================-->
    <footer class="footer_area section_gap_top">
        <div class="container">
        
            <div class="row single-footer-widget">
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="copy_right_text">
                        <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script type="c62a19f8773acdae2ac8cd25-text/javascript">document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="/https://colorlib.com/" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="social_widget">
                        <a href="/#"><i class="fa fa-facebook"></i></a>
                        <a href="/#"><i class="fa fa-twitter"></i></a>
                        <a href="/#"><i class="fa fa-dribbble"></i></a>
                        <a href="/#"><i class="fa fa-behance"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--================End Footer Area =================-->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="js/jquery-3.2.1.min.js" type="c62a19f8773acdae2ac8cd25-text/javascript"></script>
    <script src="js/popper.js" type="c62a19f8773acdae2ac8cd25-text/javascript"></script>
    <script src="js/bootstrap.min.js" type="c62a19f8773acdae2ac8cd25-text/javascript"></script>
    <script src="js/stellar.js" type="c62a19f8773acdae2ac8cd25-text/javascript"></script>
    <script src="js/jquery.magnific-popup.min.js" type="c62a19f8773acdae2ac8cd25-text/javascript"></script>
    <script src="vendors/nice-select/js/jquery.nice-select.min.js" type="c62a19f8773acdae2ac8cd25-text/javascript"></script>
    <script src="vendors/isotope/imagesloaded.pkgd.min.js" type="c62a19f8773acdae2ac8cd25-text/javascript"></script>
    <script src="vendors/isotope/isotope-min.js" type="c62a19f8773acdae2ac8cd25-text/javascript"></script>
    <script src="vendors/owl-carousel/owl.carousel.min.js" type="c62a19f8773acdae2ac8cd25-text/javascript"></script>
    <script src="js/jquery.ajaxchimp.min.js" type="c62a19f8773acdae2ac8cd25-text/javascript"></script>
    <script src="vendors/counter-up/jquery.waypoints.min.js" type="c62a19f8773acdae2ac8cd25-text/javascript"></script>
    <script src="vendors/counter-up/jquery.counterup.min.js" type="c62a19f8773acdae2ac8cd25-text/javascript"></script>
    <script src="js/mail-script.js" type="c62a19f8773acdae2ac8cd25-text/javascript"></script>
    <!--gmaps Js-->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjCGmQ0Uq4exrzdcL6rvxywDDOvfAu6eE" type="c62a19f8773acdae2ac8cd25-text/javascript"></script>
    <script src="js/gmaps.min.js" type="c62a19f8773acdae2ac8cd25-text/javascript"></script>
    <script src="js/theme.js" type="c62a19f8773acdae2ac8cd25-text/javascript"></script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13" type="c62a19f8773acdae2ac8cd25-text/javascript"></script>
<script  src= "http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="c62a19f8773acdae2ac8cd25-text/javascript">
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');

</script>
 @yield('script')

<!-- Mirrored from colorlib.com/preview/theme/comodo/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 20 May 2019 01:03:13 GMT -->
</html>