<!doctype html>
<html lang="en">


<!-- Mirrored from colorlib.com/preview/theme/comodo/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 20 May 2019 01:03:48 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
    <!-- Required meta tags -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="/img/favicon.png" type="image/png">
    <title>CDS TEUSDAY</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/css/bootstrap.css">
    <link rel="stylesheet" href="/vendors/linericon/style.css">
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/vendors/owl-carousel/owl.carousel.min.css">
    <link rel="stylesheet" href="/css/magnific-popup.css">
    <link rel="stylesheet" href="/vendors/nice-select/css/nice-select.css">
    <link rel="stylesheet" href="/vendors/animate-css/animate.css">
    <link rel="stylesheet" href="/vendors/flaticon/flaticon.css">
    <!-- main css -->
    <link rel="stylesheet" href="/css/style.css">
</head>

<body>

    <div id="app">
         <router-view></router-view>
     </div>
 
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{{ mix('js/app.js') }}" type="text/javascript"></script>
    <script src="/js/jquery-3.2.1.min.js" type="fac4bd6be35cd23db4ab693d-text/javascript"></script>
    <script src="/js/popper.js" type="fac4bd6be35cd23db4ab693d-text/javascript"></script>
    <script src="/js/bootstrap.min.js" type="fac4bd6be35cd23db4ab693d-text/javascript"></script>
    <script src="/js/stellar.js" type="fac4bd6be35cd23db4ab693d-text/javascript"></script>
    <script src="/js/jquery.magnific-popup.min.js" type="fac4bd6be35cd23db4ab693d-text/javascript"></script>
    <script src="/vendors/nice-select/js/jquery.nice-select.min.js" type="fac4bd6be35cd23db4ab693d-text/javascript"></script>
    <script src="/vendors/isotope/imagesloaded.pkgd.min.js" type="fac4bd6be35cd23db4ab693d-text/javascript"></script>
    <script src="/vendors/isotope/isotope-min.js" type="fac4bd6be35cd23db4ab693d-text/javascript"></script>
    <script src="/vendors/owl-carousel/owl.carousel.min.js" type="fac4bd6be35cd23db4ab693d-text/javascript"></script>
    <script src="/js/jquery.ajaxchimp.min.js" type="fac4bd6be35cd23db4ab693d-text/javascript"></script>
    <script src="/vendors/counter-up/jquery.waypoints.min.js" type="fac4bd6be35cd23db4ab693d-text/javascript"></script>
    <script src="/vendors/counter-up/jquery.counterup.min.js" type="fac4bd6be35cd23db4ab693d-text/javascript"></script>
    <script src="/js/mail-script.js" type="fac4bd6be35cd23db4ab693d-text/javascript"></script>
    <!--gmaps Js-->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjCGmQ0Uq4exrzdcL6rvxywDDOvfAu6eE" type="fac4bd6be35cd23db4ab693d-text/javascript"></script>
    <script src="/js/gmaps.min.js" type="fac4bd6be35cd23db4ab693d-text/javascript"></script>
    <script src="/js/theme.js" type="fac4bd6be35cd23db4ab693d-text/javascript"></script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13" type="fac4bd6be35cd23db4ab693d-text/javascript"></script>
<script type="fac4bd6be35cd23db4ab693d-text/javascript">
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
</script>

</body>


<!-- Mirrored from colorlib.com/preview/theme/comodo/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 20 May 2019 01:03:48 GMT -->
</html>

