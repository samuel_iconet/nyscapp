@extends('layouts.dashboard')


@section('content')
  <section class="home_banner_area">
        <div class="banner_inner">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="home_left_img">
                            <!-- <img class="img-fluid" src="img/banner/cash1.jpg" alt=""> -->
                        </div>
                    </div>
                    <div class="col-lg-6">

                        <div class="banner_content">
                             @if(session()->has('message'))
                                <h4 class="alert alert-success">{{session()->get('message')}}</h4>
                                @elseif(session()->has('error'))
                                <h4 class="alert alert-danger">{{session()->get('error')}}</h4>
                              @endif
                                @if(count($errors)>0)
                                    <div class="alert alert-danger">
                                      @foreach($errors->all() as $error)
                                      <p>{{$error}}</p>
                                      @endforeach
                                    </div>

                                    @endif
                                <hr><hr>
                                 <div id="confirm">
                                     <div class="message">This is a warning message.</div>
                                        <button class="yes">OK</button>
                                     </div>
                            <h3 class="alert alert-success" style="margin-top: 100px ">RESET PASSWORD</h3>
                            <form >
                                
                                 <input class="form-control" type="hidden" name="email" id="email" value="{{$email}}">
                                   <input class="form-control" type="hidden" name="token" id="token" value="{{$token}}"  >
                                   <input class="form-control" type="password" id="password" name="password" placeholder="password"  style="margin-top: 15px " >
                                   <input class="form-control" type="password" id="repassword" name="password_confirmation" placeholder="password"  style="margin-top: 15px ">
                                   
                              
                                <center  style="margin-top: 15px "><button class="primary_btn" style="margin-top: 20px; margin-bottom: 20px " id="submit"> <i id="loader" style="display: none" class="fa fa-circle-o-notch fa-spin"></i>Save Password</button></center>
                                
                            </form>
                        
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="home_left_img">
                            <!-- <img class="img-fluid" src="img/banner/cash1.jpg" alt=""> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
<script type="text/javascript">

   $(document).ready(function() {
    // $( "#err" ).hide();
   
   $( "#submit" ).click(function() {
    console.log('herll')
     $('#loader').show();
     $('#submit').attr('disabled','disabled');

    let email = $('#email').val();
    let token = $('#token').val();
    let password = $('#password').val();
    let repassword = $('#repassword').val();

    if(password != repassword){

    	var confirmBox = $("#confirm");
		            confirmBox.find(".message").text("password Mismatch, please confirm password");
		            confirmBox.find(".yes").unbind().click(function() {
		               confirmBox.hide();
           			 $('#submit').removeAttr('disabled')

		            });
		            confirmBox.show();
            	   $('#loader').hide();
            	
    }else{
    	
    console.log(token);
    console.log(email);
    console.log(password);
    console.log(repassword);
$.ajaxSetup({
                headers: { }
            });
$.post('/api/savepassword',   // url
       {     
                email: email, 
                password: password,
                 token: token
       }, // data to be submit
    // $.post('/api/token',   // url
    //    {        email: email, 
    //             password: password
               
    //    }, // data to be submit
       function(data, status, jqXHR) {// success callback

        console.log(status);
       
            if(data.code == undefined) {
            	var txt = ""; 
            	if(data.token != undefined){
            	
            	txt += data.token[0];
            	txt += ',';
            	}

            	 if((data.email != undefined)){
            		txt += data.email[0];
            		txt += ',';
            	}
            	 if((data.password != undefined)){
            		txt += data.password[0];
            		txt += ',';
            	}
            	

            	var confirmBox = $("#confirm");
		            confirmBox.find(".message").text(txt);
		            confirmBox.find(".yes").unbind().click(function() {
		               confirmBox.hide();
		            });
		            confirmBox.show();
            	   $('#loader').hide();
           			$('#submit').removeAttr('disabled');
            	 }else if(data.code == 200){

            	 	var confirmBox = $("#confirm");
		            confirmBox.find(".message").text("You password have been changed, you can now login to your account");
		            confirmBox.find(".yes").unbind().click(function() {
		               confirmBox.hide();
           			 $('#submit').removeAttr('disabled')
           			 window.location.href = '/login';

		               
		            });
		            confirmBox.show();
            	   $('#loader').hide();
            	 }else{
            	 	var confirmBox = $("#confirm");
		            confirmBox.find(".message").text("Invalid or Expired token");
		            confirmBox.find(".yes").unbind().click(function() {
		               confirmBox.hide();
           			 $('#submit').removeAttr('disabled')
           			 window.location.href = '/login';

		               
		            });
		            confirmBox.show();
            	   $('#loader').hide();
            	 }
           


        }).fail(function(jqxhr, settings, ex) {
          
          	var confirmBox = $("#confirm");
		            confirmBox.find(".message").text("Server Error, please check internet connectivity.");
		            confirmBox.find(".yes").unbind().click(function() {
		               confirmBox.hide();
           			 $('#submit').removeAttr('disabled')
		               
		            });
		            confirmBox.show();
           $('#loader').hide();
           $('#submit').removeAttr('disabled');

         });
    }
   




});
});
  </script>
  @endsection