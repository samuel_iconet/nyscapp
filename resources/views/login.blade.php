@extends('layouts.dashboard')


@section('content')

	<!--================Home Banner Area =================-->
	<section class="home_banner_area">
		<div class="banner_inner">
			<div class="container">
				<div class="row">
					<div class="col-lg-3">
						<div class="home_left_img">
							<!-- <img class="img-fluid" src="img/banner/cash1.jpg" alt=""> -->
						</div>
					</div>
					<div class="col-lg-6">

						<div class="banner_content">
					         <div id="confirm">
                                     <div class="message">This is a warning message.</div>
                                        <button class="yes">OK</button>
                                     </div>
								<hr><hr>
							
							<h3 class="alert alert-success" style="margin-top: 100px ">Login</h3>
									  @if(session()->has('message'))
					                <h4 class="alert alert-success">{{session()->get('message')}}</h4>
					              @endif			

					                @if(session()->has('error'))
					                <h4 class="alert alert-danger">{{session()->get('error')}}</h4>
					              @endif
					                        @if(count($errors)>0)
					                        <div class="alert alert-danger">
					                          @foreach($errors->all() as $error)
					                          <p>{{$error}}</p>
					                          @endforeach
					                        </div>

					                        @endif

							<form>
								<input type="text" class="form-control" id="email" name="" placeholder="Email" style="margin-top: 40px ">
								<input type="password" class="form-control" id="password" name="" placeholder="password" style="margin-top: 15px ">

								<center><button class="primary_btn" style="margin-top: 20px; margin-bottom: 20px " id="submit"><i id="loader" style="display: none" class="fa fa-circle-o-notch fa-spin"></i> Login</button></center>
							<span  class="pull-right">Dont Have an Acount ? <a href="/register">REGISTER</a> || <a href="/reset">Forgot Password?</a></span> <hr>
								
							</form>
						
						</div>
					</div>
					<div class="col-lg-3">
						<div class="home_left_img">
							<!-- <img class="img-fluid" src="img/banner/cash1.jpg" alt=""> -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--================End Home Banner Area =================-->
	<!--================ End Pricing Plans Area ================-->
@endsection

@section('script')
  <script type="text/javascript">

      $( "#submit" ).click(function() {
     $('#loader').show();
     $('#submit').attr('disabled','disabled');
    let email = $('#email').val();
    let password = $('#password').val();
    let token2 = '';
    console.log(email);
    console.log(password);
$.ajaxSetup({
                headers: { }
            });
$.post('/oauth/token',   // url
       {        username: email, 
                password: password,
                grant_type: "password",
                client_id: "2",
                client_secret: "nveN3wHJZVSmEGsdap6odzBKghunUkzcWDFNzQhs"
       }, // data to be submit
    // $.post('/api/token',   // url
    //    {        email: email, 
    //             password: password
               
    //    }, // data to be submit
       function(data, status, jqXHR) {// success callback

        console.log(status);
               // console.log(data.access_token);
              localStorage.setItem('access_token', data.access_token);
               token2  = data.access_token;
                  console.log(token2);
                 token2 = "Bearer "  + token2;
                  console.log(token2);
                  $.ajax({
                  type: "GET",
                  headers: {'Content-Type': 'application/json', Authorization: token2  },
                  url: "/api/user",
                  success: function(patientDTO) {
                      console.log("SUCCESS: ", patientDTO);
                                  localStorage.setItem('name', patientDTO.name);
                                 localStorage.setItem('email', patientDTO.email);
                                 localStorage.setItem('id', patientDTO.id);
                                 localStorage.setItem('created_at', patientDTO.created_at);
                                      window.location.href = '/dashboard';
                       },
                  error: function(e) {
                    
                     var confirmBox = $("#confirm");
		            confirmBox.find(".message").text(e);
		            confirmBox.find(".yes").unbind().click(function() {
		               confirmBox.hide();
		            });
		            confirmBox.show();
            	   $('#loader').hide();
           			$('#submit').removeAttr('disabled');
                  // display(e.responseJSON.message);
                  // alert(e);

                  }
              });



        }).fail(function(jqxhr, settings, ex) {
          if(ex == 'Unauthorized'){
          	var confirmBox = $("#confirm");
		            confirmBox.find(".message").text("Authentication Error");
		            confirmBox.find(".yes").unbind().click(function() {
		               confirmBox.hide();
           			 $('#submit').removeAttr('disabled')
		               
		            });
		            confirmBox.show();
           $('#loader').hide();
           $('#submit').removeAttr('disabled');
          }else{
          	var confirmBox = $("#confirm");
		            confirmBox.find(".message").text("Server Error, please check internet connectivity.");
		            confirmBox.find(".yes").unbind().click(function() {
		               confirmBox.hide();
           			 $('#submit').removeAttr('disabled')
		               
		            });
		            confirmBox.show();
           $('#loader').hide();
           $('#submit').removeAttr('disabled');
          }
         });




});

  </script>

@endsection