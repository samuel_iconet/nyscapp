@extends('layouts.dashboard')


@section('content')

	<section class="home_banner_area">
		<div class="banner_inner">
			<div class="container">
				<div class="row">
					<div class="col-lg-3">
						<div class="home_left_img">
							<!-- <img class="img-fluid" src="img/banner/cash1.jpg" alt=""> -->
						</div>
					</div>
					<div class="col-lg-6">

						<div class="banner_content">
							 <div id="msg"></div>
							 <div id="confirm">
							         <div class="message">This is a warning message.</div>
							            <button class="yes">OK</button>
							         </div>
								<hr><hr>

							<h3 class="alert alert-success" style="margin-top: 100px ">Register an Acount</h3>
							<form>
								<input type="text" class="form-control" id="name" name="" placeholder="Name" style="margin-top: 10px ">
								<input type="email" class="form-control" id="email" name="" placeholder="Email" style="margin-top: 15px ">
								<input type="number" class="form-control" id="number" name="" placeholder="Phone Number" style="margin-top: 15px ">
								<input type="password" class="form-control" id="password" name="" placeholder="password" style="margin-top: 15px ">
								<input type="password" class="form-control"  id="repassword" name="" placeholder="Retype password" style="margin-top: 15px ">

								<center><button class="primary_btn" style="margin-top: 20px; margin-bottom: 20px " id="submit" >  <i id="loader" style="display: none" class="fa fa-circle-o-notch fa-spin"></i>Register</button></center>
							<span  class="pull-right">Already Have an Acount <a href="/login">Login</a></span> <hr>
								
							</form>
						
						</div>
					</div>
					<div class="col-lg-3">
						<div class="home_left_img">
							<!-- <img class="img-fluid" src="img/banner/cash1.jpg" alt=""> -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--================End Home Banner Area =================-->
	<!--================ End Pricing Plans Area ================-->
@endsection
@section('script')
  <script type="text/javascript">

   $(document).ready(function() {
    // $( "#err" ).hide();
   
   $( "#submit" ).click(function() {
     $('#loader').show();
     $('#submit').attr('disabled','disabled');
    let name = $('#name').val();
    let email = $('#email').val();
    let password = $('#password').val();
    let repassword = $('#repassword').val();
    let number = $('#number').val();
    
    if(password != repassword){

    	var confirmBox = $("#confirm");
		            confirmBox.find(".message").text("password Mismatch, please confirm password");
		            confirmBox.find(".yes").unbind().click(function() {
		               confirmBox.hide();
           			 $('#submit').removeAttr('disabled')

		            });
		            confirmBox.show();
            	   $('#loader').hide();
            	
    }else{
    	 console.log(name);
    console.log(number);
    console.log(email);
    console.log(password);
    console.log(repassword);
$.ajaxSetup({
                headers: { }
            });
$.post('/api/register',   // url
       {        name: name, 
                email: email, 
                password: password,
                 number: number
       }, // data to be submit
    // $.post('/api/token',   // url
    //    {        email: email, 
    //             password: password
               
    //    }, // data to be submit
       function(data, status, jqXHR) {// success callback

        console.log(status);
       
            if(data.code == undefined) {
            	var txt = ""; 
            	if(data.name != undefined){
            	
            	txt += data.name[0];
            	txt += ',';
            	}

            	 if((data.email != undefined)){
            		txt += data.email[0];
            		txt += ',';
            	}
            	 if((data.password != undefined)){
            		txt += data.password[0];
            		txt += ',';
            	}
            	 if((data.number != undefined)){
            		txt += data.number[0];
            		txt += ',';
            	}

            	var confirmBox = $("#confirm");
		            confirmBox.find(".message").text(txt);
		            confirmBox.find(".yes").unbind().click(function() {
		               confirmBox.hide();
		            });
		            confirmBox.show();
            	   $('#loader').hide();
           			$('#submit').removeAttr('disabled');
            	 }else if(data.code == 200){

            	 	var confirmBox = $("#confirm");
		            confirmBox.find(".message").text("Welcome to Groove, your Registeration was successfully, A verification link has beeb sent to your Email please login to verify.");
		            confirmBox.find(".yes").unbind().click(function() {
		               confirmBox.hide();
           			 $('#submit').removeAttr('disabled')
           			 window.location.href = '/';

		               
		            });
		            confirmBox.show();
            	   $('#loader').hide();
            	 }
           


        }).fail(function(jqxhr, settings, ex) {
          
          	var confirmBox = $("#confirm");
		            confirmBox.find(".message").text("Server Error, please check internet connectivity.");
		            confirmBox.find(".yes").unbind().click(function() {
		               confirmBox.hide();
           			 $('#submit').removeAttr('disabled')
		               
		            });
		            confirmBox.show();
           $('#loader').hide();
           $('#submit').removeAttr('disabled');

         });
    }
   




});
});
  </script>

@endsection