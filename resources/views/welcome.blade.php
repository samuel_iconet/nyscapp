<!doctype html>
<html lang="en">


<!-- Mirrored from colorlib.com/preview/theme/comodo/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 20 May 2019 01:03:48 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="/img/favicon.png" type="image/png">
    <title>CDS TEUSDAY</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/css/bootstrap.css">
    <link rel="stylesheet" href="/vendors/linericon/style.css">
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/vendors/owl-carousel/owl.carousel.min.css">
    <link rel="stylesheet" href="/css/magnific-popup.css">
    <link rel="stylesheet" href="/vendors/nice-select/css/nice-select.css">
    <link rel="stylesheet" href="/vendors/animate-css/animate.css">
    <link rel="stylesheet" href="/vendors/flaticon/flaticon.css">
    <!-- main css -->
    <link rel="stylesheet" href="/css/style.css">
</head>

<body>

    <!--================Header Menu Area =================-->
    <header class="header_area">
        <div class="main_menu">
            <nav class="navbar navbar-expand-lg navbar-light">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <a class="navbar-brand logo_h" href="/index-2.html"><img src="/img/nysclogo.png" alt="" style="width: 110px; height: 100px"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                     aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
                        <ul class="nav navbar-nav menu_nav justify-content-center">
                        <h1 style="color: white">EPS TUESDAY CDS - WHATSAPP VERIFICATION PAGE</h1>
                        
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <!-- <li class="nav-item"><a href="/#" class="primary_btn">login</a></li> -->
                        
                            
                        </ul>
                        <!-- <form class="form-inline">
                              <div class="form-group mx-sm-3 mb-2">
                                <label for="inputPassword2" class="sr-only">Email</label>
                                <input type="email"  class="form-control" id="staticEmail2" placeholder="Email">
                              </div>
                              <div class="form-group mx-sm-3 mb-2">
                                <label for="inputPassword2" class="sr-only">Password</label>
                                <input type="password" class="form-control" id="inputPassword2" placeholder="Password">
                              </div>
                              <button type="submit" class="primary_btn mb-2">Login</button>
                      </form> -->
                    </div>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="nav-item submenu dropdown active">
                                    <a href="/#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                                    aria-expanded="false">Jane Pat </a>
                                    <ul class="dropdown-menu">
                                        <li class="nav-item"><a class="nav-link" href="/price.html">Add New User</a>
                                        <li class="nav-item"><a class="nav-link" href="/games.html">Whatsap Link</a>
                                        <li class="nav-item"><a class="nav-link" href="/elements.html">Settings</a>
                                    </ul>
                                </li>
                    </ul>
                </div>
            </nav>
        </div>
    </header>
    <!--================Header Menu Area =================-->

    <!--================Home Banner Area =================-->
    <section class="home_banner_area" > <!-- // style="margin-top: 160px;height: 450px !important"> -->
        <div class="banner_inner">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="home_left_img">
                            <img class="img-fluid" src="/img/phone.png" alt="" style="width: 600px; height: 300px">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="banner_content">
                        <h3 style="margin-top: 60px">
                                Enter Your State Code and Whatsapp Phone Number to Join The Whatsapp Group.
                            </h3>
                            <p>
                                This is strictly for corp members assigned to EPS tuesday, if ou have any issue join please message us using the contact us form.
                            </p>
                            <form> 
                                <center><input type="text" name="" class="form-control" placeholder="State Code" style="margin-top: 50px; width: 250px"> </center>
                                <center><input type="number" name="" class="form-control" placeholder="Whatsapp Number" style="margin-top: 10px;margin-bottom: 20px; width: 250px"></center>
                                <center><a href="/#" class="primary_btn">Verify</a></center>
                            <!--    <div class="row">
                                    <div class="col-lg-1">
                                        
                                    </div>
                                    <div class="col-lg-8">
                                        <input type="text" name="" class="form-control" placeholder="State Code" > 
                                        <input type="number" name="" class="form-control" placeholder="Whatsapp Number" style="margin-top: 10px"> 
                                    </div>
                                    <div class="col-lg-3">
                                        
                                    </div>
                                    
                                </div> -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================End Home Banner Area =================-->

    <!--================Start About Us Area =================-->
    <section class="about_us_area section_gap_top">
        <div class="container">
            <div class="row about_content align-items-center">
                <div class="col-lg-12">
                    <div class="section_content">
                        <h6>Contact Us</h6>
                        <h1>If you have experience any issue please contact Us NOW!!!</h1>
                        <div class="row"> 
                            
                                <div class="col-xl-6 offset-2">
                                    <form>
                                    <input type="text" class="form-control" name="Name" placeholder="Name" >
                                    <input type="email" class="form-control" name="Email" placeholder="Email" style="margin-top: 10px">
                                    <textarea class="form-control" style="margin-top: 10px"  placeholder="message"></textarea>
                                    <center><button style="margin-top: 10px"  class="primary_btn">Send</button></center>
                                    </form>
                                </div>
                            
                        
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    <!--================End About Us Area =================-->


    

    <!--================Footer Area =================-->
    <footer class="footer_area section_gap_top">
        <div class="container">
    
            <div class="row single-footer-widget">
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="copy_right_text">
                        <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script type="fac4bd6be35cd23db4ab693d-text/javascript">document.write(new Date().getFullYear());</script> All rights reserved | EPS TUESDAY || WHATSAPP VRIFICATION PAGE </p>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12">
                    
                </div>
            </div>
        </div>
    </footer>
    <!--================End Footer Area =================-->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="/js/jquery-3.2.1.min.js" type="fac4bd6be35cd23db4ab693d-text/javascript"></script>
    <script src="/js/popper.js" type="fac4bd6be35cd23db4ab693d-text/javascript"></script>
    <script src="/js/bootstrap.min.js" type="fac4bd6be35cd23db4ab693d-text/javascript"></script>
    <script src="/js/stellar.js" type="fac4bd6be35cd23db4ab693d-text/javascript"></script>
    <script src="/js/jquery.magnific-popup.min.js" type="fac4bd6be35cd23db4ab693d-text/javascript"></script>
    <script src="/vendors/nice-select/js/jquery.nice-select.min.js" type="fac4bd6be35cd23db4ab693d-text/javascript"></script>
    <script src="/vendors/isotope/imagesloaded.pkgd.min.js" type="fac4bd6be35cd23db4ab693d-text/javascript"></script>
    <script src="/vendors/isotope/isotope-min.js" type="fac4bd6be35cd23db4ab693d-text/javascript"></script>
    <script src="/vendors/owl-carousel/owl.carousel.min.js" type="fac4bd6be35cd23db4ab693d-text/javascript"></script>
    <script src="/js/jquery.ajaxchimp.min.js" type="fac4bd6be35cd23db4ab693d-text/javascript"></script>
    <script src="/vendors/counter-up/jquery.waypoints.min.js" type="fac4bd6be35cd23db4ab693d-text/javascript"></script>
    <script src="/vendors/counter-up/jquery.counterup.min.js" type="fac4bd6be35cd23db4ab693d-text/javascript"></script>
    <script src="/js/mail-script.js" type="fac4bd6be35cd23db4ab693d-text/javascript"></script>
    <!--gmaps Js-->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjCGmQ0Uq4exrzdcL6rvxywDDOvfAu6eE" type="fac4bd6be35cd23db4ab693d-text/javascript"></script>
    <script src="/js/gmaps.min.js" type="fac4bd6be35cd23db4ab693d-text/javascript"></script>
    <script src="/js/theme.js" type="fac4bd6be35cd23db4ab693d-text/javascript"></script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13" type="fac4bd6be35cd23db4ab693d-text/javascript"></script>
<script type="fac4bd6be35cd23db4ab693d-text/javascript">
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
</script>

</body>


<!-- Mirrored from colorlib.com/preview/theme/comodo/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 20 May 2019 01:03:48 GMT -->
</html>