<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
// Route::get('/test', function () {
//     return view('resetpassword');
// });
// Route::get('/login', function () {
//     return view('login');
// });
// Route::get('/register', function () {
//     return view('register');
// });
// Route::get('/reset', function () {
//     return view('forgotpass');
// });
// Route::get('/passwordreset/{email}/{token}', 'Api\authcontroller@resetpassword');
// Route::get('/verification/{user_id}/{remember_token}', 'Api\authcontroller@validatemail');
// Route::get('/lunchreset/{email}/{token}', 'Api\authcontroller@lunchreset');
Route::post('/validateuser', 'HomeController@validateuser');
Route::get('{path}', "HomeController@index")->where('path','([A-z\d-\/_.]+)?');