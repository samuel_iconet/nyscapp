<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/getusers', 'systemcontroller@index');
Route::post('/addmember', 'systemcontroller@addmember');
Route::post('/editmember', 'systemcontroller@editmember');
Route::post('/deletemember', 'systemcontroller@deletemember');
Route::post('/adduser', 'systemcontroller@adduser');
Route::post('/savelink', 'systemcontroller@savelink');
