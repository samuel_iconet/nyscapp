<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\password_reset;
class passwordresetMail extends Mailable
{
    use Queueable, SerializesModels;
    public $url;
    public $pass_reset;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(password_reset $pass_reset)
    {
        //
        $this->pass_reset = $pass_reset;
        $this->url =  url('/');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
          return $this->view('email.passwordresetMail');
    }
}
