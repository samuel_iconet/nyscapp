<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\reg_user;
use App\User;
use App\whatsapp_link;
use Validator;

class systemcontroller extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    public function index(){
     $coremember = reg_user::all();
     $whatsapp_link = whatsapp_link::find(1);

     $responce['coremembers'] = $coremember;
     $responce['whatsapp_link'] = $whatsapp_link->link;
     $responce['code'] = 200;
     return response()->json( $responce,200);
    }
    public function addmember(request $request){
    	$validator = Validator::make($request->all(), [
            "state_code" =>  "required",
     		'status' => 'required',
       ]);
        
       if ($validator->fails()) {
          
            return $validator->messages();
       }
       $user = new reg_user;
       $user->state_code = $request->state_code;
       $user->phone_number = $request->phone_number;
       $user->status = $request->status;
       $user->save();
       $responce['code'] = 200;
     return response()->json( $responce,200);

    }   
     public function editmember(request $request){
    	$validator = Validator::make($request->all(), [
            "state_code" =>  "required",
     		'status' => 'required',
     		'id' => 'required'
       ]);
        
       if ($validator->fails()) {
          
            return $validator->messages();
       }
       $user = reg_user::find($request->id);
       if(isset($user)){
       	  $user->state_code = $request->state_code;
       $user->status = $request->status;
       $user->save();
       $responce['code'] = 200;
     return response()->json( $responce,200);
 }else{

       $responce['code'] = 202;
       $responce['error'] = "invalid details provided";

     return response()->json( $responce,200);
    }
 } 
 public function deletemember(request $request){
    	$validator = Validator::make($request->all(), [
     		'id' => 'required'
       ]);
        
       if ($validator->fails()) {
          
            return $validator->messages();
       }
       $user = reg_user::find($request->id);
       if(isset($user)){
       	  $user->delete();
       $responce['code'] = 200;
     return response()->json( $responce,200);
 }else{

       $responce['code'] = 202;
       $responce['error'] = "invalid details provided";

     return response()->json( $responce,200);
    }
 }

 public function adduser(request $request){
 	$validator = Validator::make($request->all(), [
     		'name' => 'required',
     		'email' =>'required|string|email|max:255|unique:users',
     		'password' => 'required|string|min:6|'
       ]);
        
       if ($validator->fails()) {
          
            return $validator->messages();
       }
       $user = new User;
       $user->name = $request->name;
    $user->email = $request->email;
    $user->status = '1';
    $user->password = bcrypt($request->password);
    $user->save();

  
    $response['code'] = 200;
    return response()->json($response ,200);
 }
 public function savelink(request $request){
 	$validator = Validator::make($request->all(), [
     		'group_link' => 'required',
     		
       ]);
        
       if ($validator->fails()) {
          
            return $validator->messages();
       }	
       $whatsapp_link = whatsapp_link::find(1);
       $whatsapp_link->link = $request->group_link;
       $whatsapp_link->save();
       $response['code'] = 200;
    return response()->json($response ,200);
 }
}
