<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\reg_user;
use App\whatsapp_link;
use Validator;

class HomeController extends Controller
{
    public function index(){
    	 return view('layouts.app');
    }
    public function validateuser(Request $request){
    	$validator = Validator::make($request->all(), [
     		'state_code' => 'required',
     		'phone_number' =>'required'
       ]);
        
       if ($validator->fails()) {
          
            return $validator->messages();
       }
       $user = reg_user::where('state_code' , $request->state_code)->first();
       if (isset($user)){
       	$user->phone_number = $request->phone_number;
       	$user->save();
       	$link = whatsapp_link::find(1);

       	 $response['code'] = 200;
       	 $response['link'] = $link->link;
       return response()->json($response ,200);
       }else{
       	 $response['code'] = 202;
       	 $response['error'] = "State Code not registered on this System.";
    return response()->json($response ,200);
       }
    }
}
