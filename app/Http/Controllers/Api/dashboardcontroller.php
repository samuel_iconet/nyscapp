<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Support\Str;

use Auth;
use App\ph_table;
use App\gh_table;
use App\User;
use App\merge_table;

class dashboardcontroller extends Controller
{
          public function __construct()
    {
        $this->middleware('auth:api');

   
    }
    public function homedata(){
        if(!$this->isActive(Auth::User())){
        	$responce['code'] = 500;
        	return response()->json($responce,200);

        }
    	$user_id = Auth::User()->id;
    	$phs = ph_table::where('user_id',$user_id)->get();
    	$ghs = gh_table::where('user_id',$user_id)->get();
    	$merges = merge_table::where('gh_userID' , $user_id )->orWhere( 'ph_userID' , $user_id)->get();

    	foreach ($phs as $ph) {
    		$ph['type'] = 'ph';
    		$ph['key'] = 'ph'.$ph->id;
    		$ph['user'] =  User::find($ph->user_id);
     	}
     	foreach ($ghs as $gh) {
    		$gh['type'] = 'gh';
    		$ph['key'] = 'gh'.$gh->id;
    		$gh['user'] =  User::find($gh->user_id);
     	}

     	foreach ($merges as $merge) {
     	
     	$pher = User::find($merge->ph_userID);
     	$gher = User::find($merge->gh_userID);
     	$merge['pher'] =$pher;
     	$merge['gher'] =$gher;
     	  }
    	$data = array_merge($phs->toArray(), $ghs->toArray());
    	$help = array_values(array_sort($data , function ($value) {
			    return $value['created_at'];
			}));
    	$help = array_reverse($help);
    	$responce['code'] = 200;
    	$responce['help'] = $help;
    	// $responce['ghs'] = $ghs;
    	$responce['merges'] = $merges;
    	return response()->json($responce,200);


    }
    public function providehelp(Request $request){
    		$validator = Validator::make($request->all(), [
            "amount" =>  "required",
            "percentage" =>  "required",
     		
       ]);
        
       if ($validator->fails()) {
          
            return $validator->messages();
       }

       $ph = new ph_table;
       $ph->user_id = Auth::User()->id;
       $ph->amount = $request->amount;
       $ph->amount = $request->amount;
       $ph->cummulative_amount = $request->amount;
       $ph->expected_amount = (int)$request->amount + ((int)$request->amount * (int)$request->percentage / 100);
       $ph->merged_amount = 0;
       $ph->balance = $request->amount;
       $ph->type = $request->percentage;
       $ph->status = 'Pending';
       $ph->ph_key = md5(microtime().Str::random(5));
       $date = date("Y-m-d");
       if($request->percentage == "50"){
       	 $ph->release_date = date('Y-m-d', strtotime($date. ' + 5 days'));
       }
       else{
       	 $ph->release_date = date('Y-m-d', strtotime($date. ' + 10 days'));
       }
       $ph->save();
       	$responce['code'] = 200;
       	$responce['message'] = "You have succesfully Created a PH request.";
    	return response()->json($responce,200);


    }
    public function cancelph(request $request){
    		$validator = Validator::make($request->all(), [
            "id" =>  "required"
     		
       ]);
        
       if ($validator->fails()) {
          
            return $validator->messages();
       }

       $ph = ph_table::find($request->id);
       if(isset($ph)){

       	 if($ph->user_id == Auth::User()->id){
       	 	$ph->delete();
       	 	$responce['code'] = 200;
       	$responce['message'] = "Deleted succesfully.";
    	return response()->json($responce,200);
       	 }
       	 else{
       	 	$responce['code'] = 203;
       	$responce['error'] = "Permission denied";
    	return response()->json($responce,200);

       	 }
       }else{
       	$responce['code'] = 202;
       	$responce['erro'] = "Invalid PD ID";
    	return response()->json($responce,200);
       }
    } 

    public function isActive(User $user){
    	if ($user->status == '0'){
    		return false;
    	}
    	else{
    		return true;
    	}
    }
}
