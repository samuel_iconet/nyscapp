<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\WelcomeMail;
use App\Mail\passwordresetMail;
use Validator;
use App\User;
use Session;
use App\password_reset;
use Illuminate\Support\Str;

class authcontroller extends Controller
{
    public function register(request $request){
    	$validator = Validator::make($request->all(), [
            "name" =>  "required",
     		'email' => 'required|string|email|max:255|unique:users',
     		'number' => 'required|max:11|min:11',
    		'password' => 'required|string|min:6|'
       ]);
        
       if ($validator->fails()) {
          
            return $validator->messages();
       }
       else {
        $user = new User;
   
	    $user->name = $request->name;
	    $user->email = $request->email;
	    $user->number = $request->number;
	    $user->password = bcrypt($request->password);
	    $user->remember_token = Str::random(40);;
	    $user->status = 0;
	    $user->save();

  	  \Mail::to($user)->send(new WelcomeMail($user));
    $response['code'] = 200;
    $response['message'] = "Registeration successfull, A confirmation Mail has been sent to your Email, Please Confirm Registration";
    $response['user_id'] =$user->id;
    return response()->json($response ,200);
       }
    }
    public function validatemail($user_id, $remember_token){
    	$user = User::findorFail($user_id);
    	if($user->remember_token == $remember_token){
    		$user->status = 1;
    		$user->remember_token = "";
    		$user->save();
    		 session()->flash('message','Email Validation successfull, Please Login to your account!!!');
					   	  return redirect('/login');
    		// $response['code'] = 200;
		    // $response['message'] = "Email Validation successfull.";
		    // return response()->json($response ,200);
    	}else{
    		// $response['code'] = 202;
		    // $response['message'] = "Email Validation Unsuccessful, Please Check your Mail or Request a new Verification Mail.";
		    // $response['user_id'] = $user->id; 
 		   //  return response()->json($response ,200);
    	}
    }
    public function login(request $request){
    $validator = Validator::make($request->all(), [
     		'email' => 'required|',
    		'password' => 'required|string|min:6|'
       ]);
        
       if ($validator->fails()) {
          
            return $validator->messages();
       }


    }
     public function token(request $request){
   
			     // return $request->all();
			     $this->validate($request,[
			            'email' => 'required|string|email',
			             'password' => 'required'
			         ]);
			     $user = User::where('email' , $request->email)->first();
			     if(isset($user)){
			     	if($user->status == '0' ){

						$response['code'] = 202;
						$response['error'] = "User Email not Verified, Please verify Email.";
						$response['user_id'] = $user->id;
						 return response()->json($response ,200);
			     	}
			     
			      

			      } 

			     	$curl = curl_init();
			  // Set some options - we are passing in a useragent too here
			  
			    curl_setopt_array($curl, [
			    CURLOPT_RETURNTRANSFER => true,
			    CURLOPT_ENCODING =>  "gzip",
			    CURLOPT_ENCODING => '',
			    // CURLOPT_URL => 'http://iconet.com.ng/oauth/token',
			     CURLOPT_URL => 'http://127.0.0.1:8000/oauth/token',
			    CURLOPT_USERAGENT => 'Codular Sample cURL Request',
			    CURLOPT_POST => 1,
			    CURLOPT_POSTFIELDS     => array(
			        'username' => $request->email,
			        'password' => $request->password,
			        'grant_type' => 'password',
			        'client_id' => '2',
			        'client_secret' => 'nveN3wHJZVSmEGsdap6odzBKghunUkzcWDFNzQhs',
			    )

			   
			]);
			// Send the request & save response to $resp
			$resp = curl_exec($curl);
			$result = json_decode($resp,true);
			$response = $result;
			$response['code'] = 200;
			 return response()->json($response ,200);
			  
			    


			}
			public function recoverpassword(request $request){
					$validator = Validator::make($request->all(), [
			            
			     		'email' => 'required|string|email',
			     		
			       ]);
			        
			       if ($validator->fails()) {
			          
			            return $validator->messages();
			       }

				   $user = User::where('email' ,  $request->email)->first();
				

				   if(isset($user)){
				   	$pass_avail = password_reset::where('email' , $request->email)->get();
					   	if(isset($pass_avail)){
					   		foreach ($pass_avail as $pass) {
					   			$pass->delete();
					   		}
					   	}
					   	$pass_reset =  new password_reset;
					   	$pass_reset->email = $request->email;
					   	$pass_reset->token = Str::random(40);
					   	$pass_reset->save();
					   	\Mail::to($user)->send(new passwordresetMail($pass_reset));
					   	 $response['message'] = 'Password reset Mail Sent successfully';
					   	 $response['code'] = '200';
					   	 return response()->json($response ,200);

				   }else{
				   
					 $response['error'] = 'The email provided is not registered.';
					   	 $response['code'] = '202';
					   	 return response()->json($response ,200);
					   	  				   }
			}
			public function resetpassword($email , $token){
				$pass_reset = password_reset::where(['email' => $email, 'token' => $token])->first();
				if(isset($pass_reset)){
					$response['code'] = 200;
					$response['email'] = $email;
					$response['token'] = $token;
			 	 		return view('resetpassword' , ['email' => $email , 'token' => $token] );
				}
				else{
				   session()->flash('error','invalid Token provided');
						return view('resetpassword' , ['email' => $email , 'token' => $token] );

				   }
			}
			public function savepassword(request $request){

				
				 $validator = Validator::make($request->all(), [
			            
			     		 'email' => 'required|string|email',
			            'token' => 'required',
			            'password' => 'required|string|min:6|'
			     		
			       ]);
			        
			       if ($validator->fails()) {
			          
			            return $validator->messages();
			       }

				 $email = $request->email;
				 $token =$request->token;
				 $pass_reset = password_reset::where(['email' => $email, 'token' => $token])->first();
				if(isset($pass_reset)){
					$user = User::where('email' , $request->email)->first();
					if(isset($user)){
						$user->password = bcrypt($request->password);
						$user->save();
					$pass_reset->delete();
					   	  $response['code'] = 200;
					   	  $response['message'] = 'Password  reset successful';
			 			return response()->json($response ,200);
					}
					else{
					
					  $response['code'] = 202;
					   	  $response['message'] = 'Invalid Password reset Token';
			 			return response()->json($response ,200);
					   	  return redirect()->bacK();
					}
				}
				else{
					
				  $response['code'] = 202;
					   	  $response['message'] = 'Invalid Password reset Token';
			 			return response()->json($response ,200);
					   	  return redirect()->bacK();

				   }
			}
}
